#!/usr/bin/env bash

# colors
bold=$(tput bold 2> /dev/null || echo -e "\x1b[1m")
red=$(tput setaf 1 2> /dev/null || echo -e "\x1b[38;5;1m")
green=$(tput setaf 2 2> /dev/null || echo -e "\x1b[38;5;2m")
yellow=$(tput setaf 3 2> /dev/null || echo -e "\x1b[38;5;3m")
blue=$(tput setaf 4 2> /dev/null || echo -e "\x1b[38;5;4m")
cyan=$(tput setaf 6 2> /dev/null || echo -e "\x1b[38;5;6m")
reset=$(tput sgr0 2> /dev/null || echo -e "\x1b[0m")

# misc. config
image_name="aur_builder"
if [[ "${CI_COMMIT_BRANCH}" = "master" ]]; then
	image_tag="latest"
else
	image_tag="${CI_COMMIT_REF_SLUG}"
fi

s6_version="2.0.0.1"
socklog_version="3.1.0-2"

# Use vfs with buildah. Docker offers overlayfs as a default, but buildah
# cannot stack overlayfs on top of another overlayfs filesystem.
export STORAGE_DRIVER=vfs

set -ex

echo "${green}Building container${reset}"
container=$(buildah from --pull docker.io/archlinux)

# add s6 overlay
buildah add ${container} https://github.com/just-containers/s6-overlay/releases/download/v${s6_version}/s6-overlay-amd64.tar.gz /tmp/
buildah run ${container} -- tar xzf /tmp/s6-overlay-amd64.tar.gz -C / --exclude='./bin'
buildah run ${container} -- tar xzf /tmp/s6-overlay-amd64.tar.gz -C /usr ./bin
# update and install packages
buildah run ${container} -- pacman -Syu --noconfirm
buildah run ${container} -- pacman -S --needed --noconfirm base-devel git go darkhttpd cronie
# add user for makepkg
buildah run ${container} -- useradd -Ums /bin/bash user
# build and install repoctl
buildah run --user 'user' ${container} -- sh -c 'mkdir -p ~/packages && cd ~/packages && git clone https://aur.archlinux.org/repoctl && cd repoctl && makepkg'
buildah run ${container} -- sh -c 'pacman -U --noconfirm /home/user/packages/repoctl/*.pkg.tar.zst'
buildah run ${container} -- rm -rf /home/user/packages/repoctl
# cleanup
buildah run ${container} -- pacman -Rns --noconfirm go
# create filesystem
buildah run ${container} -- mkdir -p /config /repo
buildah run ${container} -- chown -R user:user /config /repo
buildah copy ${container} root/ /
# configuration
buildah config --entrypoint '["/init"]' --cmd '' --port 8080 --env 'REPOCTL_CONFIG=/config/repoctl.toml' --env 'CLEAR_CACHE=except-vcs' --env 'CACHE_PATH=/home/user/packages' ${container}

echo "${green}Committing container${reset}"
buildah commit --format docker ${container} ${image_name}

if [[ -n "${CI_REGISTRY}" ]]; then
	echo -e "${green}Pushing image${reset}"
	buildah push ${image_name} "${CI_REGISTRY_IMAGE}:${image_tag}"
fi
