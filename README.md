# AUR builder

A docker image to automatically update and build AUR packages and host them in a pacman repository.

## Running

See [docker-compose.yml](docker-compose.yml) for an example.

### Caching package source files

Package files are deleted after building them. This behaviour can be controlled by setting the `CLEAR_CACHE` environment variable in the container to one of these values

- `always`: Delete package files for all packages after building them.
- `except-vcs`: Keep package files for VCS packages, delete package files for all other packages.
- `never`: Keep package files for all packages.

The default value is `except-vcs` so VCS packages don't have to be redownloaded on every update.

## Usage

By default a newly created container will initialize a package repository and that's it. You have to add packages to the repo using the `add-package` command explained below before the container does anything useful.

Updates are run every hour on the hour. On each update the container first checks whether packages were added with `add-package` and then downloads and builds them and their dependencies. Then all packages available in the local package repository are checked for updates and built if any are available. Finally all local VCS packages are checked for updates and built if any are available. All built packages will be added to the local package repository.

The container continually runs a `darkhttpd` process in the background to serve the entire repository over HTTP on port 8080.

Packages installed inside the container will be upgraded daily.

### Adding packages

You can add packages to the repo using `docker-compose exec aur_builder add-package [package-name ...]`. Added packages will be downloaded and built on the next update.

### Manually forcing an update

Updates can be forced by running `docker-compose exec aur_builder update`.

### Signing built packages

You can mount any valid GnuPG directory containing a keyring into the container at `/home/user/.gnupg`. The keyring must contain a GPG private key without password protection. You should use a subkey with short expiry date for this to keep the risk low. Specify the key id of the key you want to use for package signing in the `GPGKEY` environment variable for the container to sign all built packages.

The specified key will be added to pacman's trust db inside the container to allow installing locally built packages and verify the repository itself if it's signed.

### Signing the repository

If you have set up package signing, you just need to edit the `/config/repoctl.toml` file and add `"--sign"` to the `add_params` and `rm_params` array. This will enable signing the repository as well.

Should you remove these options or the `GPGKEY` environment variable, make sure to remove the `repo_name.db[.tar.zst].sig` and `repo_name.files.[tar.zst].sig` files from the repository as well. This makes sure that no old and invalid signature will be kept around.

## Using the created repository

Add the new repo to your `pacman.conf`:

```conf
[repo_name]
SigLevel = Optional TrustAll
Server = http://your-server:8080/
```

## Container logs

Package builds are logged to `/var/run/packages` using `s6-log`. System package upgrades are logged to `/var/run/upgrades` also using `s6-log`.
